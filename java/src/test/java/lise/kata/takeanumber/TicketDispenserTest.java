package lise.kata.takeanumber;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

final class TicketDispenserTest {

    @Test
    void dispensesTicketsWithConsecutiveNumbers() {
        assertThat(1 + 1).isEqualTo(2);
        assertEquals(2, 1 + 1);
    }
}