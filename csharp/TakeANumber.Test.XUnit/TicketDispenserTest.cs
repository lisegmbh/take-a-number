using FluentAssertions;
using NFluent;
using Xunit;

namespace TakeANumber.Test.XUnit
{
    public class TicketDispenserTest
    {
        [Fact]
        public void DispensesTicketsWithConsecutiveNumbers()
        {
            Check.That(1+1).IsEqualTo(2);
            (1 + 1).Should().Be(2);
        }
    }
}
