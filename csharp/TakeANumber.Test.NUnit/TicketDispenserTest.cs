using FluentAssertions;
using NFluent;
using NUnit.Framework;

namespace TakeANumber.Test.NUnit
{
    public class TicketDispenserTest
    {
        [SetUp]
        public void Setup() { }

        [Test]
        public void DispensesTicketsWithConsecutiveNumbers()
        {
            Check.That(1+1).IsEqualTo(2);
            (1 + 1).Should().Be(2);
        }
    }
}
