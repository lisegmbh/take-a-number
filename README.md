# lise - Take-A-Number

Um Warteschlangen von Kunden zu verwalten, soll eine Software für ein Wartemarkenautomaten eines Aufrufsystem iterativ entwickelt werden.

_Englische Begriffe:_
* Aufrufsystem – _queue management system_ (QMS)
* Anzeigetafel – main display
* Wartebereich – waiting area
* Wartemarkenautomat – ticket dispenser
* Wartemarke – (queue) ticket
* Wartenummer – (ticket) number

## Aufgaben

s. auch [Take-A-Number.pdf](Take-A-Number.pdf)

### #001
Als Kunde möchte ich an einem Wartemarkenautomat eine Wartemarke mit einer eindeutigen Nummer, dem heutigen Datum und der Uhrzeit ziehen können, um meinen Platz in der Warteschlage einzunehmen.

_[Aufrufnummern müssen sequentiell und eindeutig an Kunden vergeben werden.]_

### #002
Als Betreiber des Aufrufsystems möchte ich mehrere Automaten im Wartebereich aufstellen können, um ein Drängeln an einem Automaten zu vermeiden.

_[Kein Automat darf eine Aufrufnummer doppelt vergeben.]_

_[Weiterhin gilt, dass vergebene Nummern sequentiell vergeben werden müssen.]_

### #003
Als Mitarbeiter möchte ich über eine Anzeigetafel im Wartebereich die nächste Wartenummer anzeigen können, um den in der Warteschlange nächsten Kunden aufrufen zu können.

_[Das Aufrufsystem muss die Nummern in der gezogenen Reihenfolge anzeigen.]_

### #004
Als Kunde möchte ich auf der Wartemarke einen Willkommenstext sehen, um mit einem positiven Gefühl den Wartebereich zu betreten.

_[Zunächst soll je nach Uhrzeit "Guten Morgen" (bis 10 Uhr), "Guten Tag" (zwischen 10 und 17 Uhr) bzw. "Guten Abend" (nach 17 Uhr) auf den Wartemarken stehen]_

### #005
Als Betreiber möchte ich wissen, was die durchschnittliche Wartezeit der Kunden ist.

_[Als Eingabe für die Berechnung soll eine Liste an Wartemarken und Aufrufzeiten dienen]_

### #006
Als Betreiber möchte ich wissen, zu welchen Stunden des Tages (Top 3 „busiest Hours“) die meisten Wartemarken gezogen worden sind.

_[Als Eingabe für die Berechnung soll eine Liste an Wartemarken dienen]_