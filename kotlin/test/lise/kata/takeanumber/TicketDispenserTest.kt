package lise.kata.takeanumber

import org.junit.jupiter.api.Test

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals

class TicketDispenserTest {

    @Test
    fun `dispenses tickets with consecutive numbers`() {
        assertThat(1 + 1).isEqualTo(2)
        assertEquals(2, 1 + 1)
    }
}